public class Application {
    
    public static void main (String args[])
    {
        Students x = new Students();
        x.studentName = "Sriraam";
        x.studentsAge = 18;
        x.studentsWithPc = "Yes";

        Students y = new Students();
        y.studentName = "Ryan";
        y.studentsAge = 17;
        y.studentsWithPc = "No";

        Students z = new Students();
        z.studentName = "Davis";
        z.studentsAge = 18;
        z.studentsWithPc = "Yes";

        Students[] section3 = new Students[3];
        section3[0] = x;
        section3[1] = y;
        section3[2] = z;


        System.out.println(section3[2].studentName);


    }
}
